<?php

namespace WePay;

use WeChat\Contracts\BasicWePay;
use WeChat\Exceptions\InvalidResponseException;
use WeChat\Exceptions\LocalCacheException;

class ProfitSharing extends BasicWePay
{
    /**
     * 添加分账接收人
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function addReceiver($options): array
    {
        $url = 'https://api.mch.weixin.qq.com/pay/profitsharingaddreceiver';
        return $this->callPostApi($url, $options);
    }

    /**
     * 请求分账
     * @throws InvalidResponseException
     * @throws LocalCacheException
     */
    public function create(array $options): array
    {
        $url = 'https://api.mch.weixin.qq.com/secapi/pay/profitsharing';
        return $this->callPostApi($url, $options, true);
    }
}